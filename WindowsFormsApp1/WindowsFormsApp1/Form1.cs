﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Бандяк Андрій Володимирович" + Environment.NewLine + "Студент 4 - го курсу спеціальності Менеджмент");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Самостійно навчався html та СSS");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Interesting article" + Environment.NewLine"https://tproger.ru/category/articles/");
        }
    }
}
